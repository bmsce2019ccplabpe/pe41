#include<stdio.h>
int input ()
{
  int a;
  printf ("enter the number\n");
  scanf ("%d", &a);
  return a;
}

int
compute (int n)
{
  int x, sum = 0;
  while (n != 0)
    {
      x = n % 10;
      sum = sum + x;
      n = n / 10;
    }
  return sum;
}

void output (int sum)
{
  printf ("the sum of digits of the numbers is %d", sum);
}

int main ()
{
  int sum, num;
  num = input ();
  sum = compute (num);
  output (sum);
  return 0;
}