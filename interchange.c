#include<stdio.h>
int
main ()
{
  int a[50], i, n, small, large, small_pos, large_pos, temp;
  printf ("enter the size of the array\n");
  scanf ("%d", &n);
  printf ("enter the arrays\n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  small = a[0];
  large = a[0];
  small_pos = 0;
  large_pos = 0;
  for (i = 1; i < n; i++)
    {
      if (a[i] < small)
	{
	  small = a[i];
	  small_pos = i;
	}
      if (a[i] > large)
	{
	  large = a[i];
	  large_pos = i;
	}
    }
  printf ("smallest number=%d at pos=%d\n", small, small_pos);
  printf ("largest number=%d at pos=%d\n", large, large_pos);
  temp = a[large_pos];
  a[large_pos] = a[small_pos];
  a[small_pos] = temp;
  for (i = 0; i < n; i++)
    printf ("%d ", a[i]);
  printf("\n");
  return 0;
}