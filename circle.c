#include<stdio.h>
float get_radius()
{
float radius;
printf("enter the radius\n");
scanf("%f",&radius);
return radius;
}
float compute_area(float radius)
{
return 3.14*radius*radius;
}
float compute_circumference(float radius)
{
return 2*3.14*radius;
}
void output_area(float radius,float area)
{
printf("the area of the circle=%f",area);
}
void output_circumference(float radius,float circumference)
{
printf("the circumference of the circle=%f",circumference);
}
int main()
{
float radius,area,circumference;
radius=get_radius();
area=compute_area(radius);
circumference=compute_circumference(radius);
output_area(radius,area);
output_circumference(radius,circumference);
return 0;
}
