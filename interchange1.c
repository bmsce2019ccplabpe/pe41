#include<stdio.h>
int
input (int a[])
{
  int i, n;
  printf ("Enter the size of the array\n");
  scanf ("%d", &n);
  printf ("Enter the elements of the array\n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  return n;
}

void
compute (int n, int a[])
{
  int smallest, largest, largest_pos, smallest_pos, temp, i;
  smallest = a[0];
  largest = a[0];
  smallest_pos = 0;
  largest_pos = 0;
  for (i = 1; i < n; i++)
    {
      if (a[i] < smallest)
	{
	  smallest = a[i];
	  smallest_pos = i;
	}
      if (a[i] > largest)
	{
	  largest = a[i];
	  largest_pos = i;
	}
    }
  printf ("smallest = %d\n", smallest);
  printf ("largest = %d\n", largest);
  temp = a[largest_pos];
  a[largest_pos] = a[smallest_pos];
  a[smallest_pos] = temp;
}

void
output (int n, int a[])
{
  int i;
  printf ("The new array is\n");
  for (i = 0; i < n; i++)
    printf ("%d  ", a[i]);
}

int
main ()
{
  int a[20], x;
  x = input (a);
  compute (x, a);
  output (x, a);
  return 0;
}